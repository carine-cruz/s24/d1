//find method
	db.collections.find({query}, {field projection})

	db.users.find()
		//returns array of documents

	/* Query Operators*/

	//Comparison Operator

	//$gt (greater than) operator
	db.users.find({age: {$gt: 50}})
		//find documents with age greater than 50

	//$gte (greater than or equal to) Operator
	db.users.find({age: {$gte: 61}})

	//$lt (less than) operator
	db.users.find({age: {$lte: 65}})

	//$lte (less than or equal to) operator
	db.users.find({age: {$lte: 65}})

	//$eq (equal) - matches values that are equal in specified value
		//default operator to query document
	db.users.find({age: {$eq: 65}})
	db.users.find({age: 65}) //same

	//$ne operator (not equal to)
		//matches all values that are not equal to the specified value
	db.users.find({age: {$ne: 82}})
	
	//$in (IN)
		//matches any values specified in an array
	db.users.find({lastName: {$in: ["Hawking", "Doe"]}})

	//find all the documents that contains courses HTML and React
	db.users.find({courses: {$in: ["HTML", "React"]}})

	//$nin
		// opposite of in. matches values which do not match values in array

	/* LOGICAL OPERATORS (and/or) */

	//AND - $and
		//Joins query clauses with a logical AND returns all documents that match the conditions of BOTH clauses.
		//default operator

		//syntax
		//{ $and: [ { <expression1> }, { <expression2> } , ... , { <expressionN> } ] }

	db.users.find(
		{
			$and: [
				{ firstName: "Niel" },
				{ age: {$gte: 82} },
				{ status: "active" }
			]
		}
	)

		//and operator evaluates all fields to be true. if one of the fields is false, it will not return documents

	db.users.find(
		{
			"firstName": "Niel",
			"age": {$gte: 82},
			"active": "status"
		}
	)
		//and operator is default behavior of query document because all fields specified in the query must be true

	//OR - $or
		//joins every clauses with a logical OR and returns all documents that match the condition of EITHER clause

	db.users.find(
		{
			$or: [{"firstName":"Neil"},{"age":30}]
		}
	)

	db.users.find({"firstName":"Neil"}) //true
	db.users.find({"age":30})			//false

	//REGULAR EXPRESSION - $regex
	// syntax: { <field>: /pattern/<options> }
	//syntax: {<field: "pattern", <options>}

	//return 0 records
	db.users.find(
		{
			"firstName": "jane"
		}
	)

		db.users.find(
		{
			"firstName": {
				$regex: "jane",
				$options: 'i'
			}
		}
	)


	/* FIELD PROJECTION PARAMETER
		specifies the fields to return in the documents that match the query filter


		//Exclusion
			//specifies the exclusion of a field. Non-zero integers are also treated as true
	 */
	 	db.users.find(
	 		{},
	 		{
	 			"_id":0,
	 			"contact":0,
	 			"department":0,
	 			"courses": 0,
	 			"status": 0
	 			"age":0,
	 			"status":0,
	 			"isAdmin":0
	 		}
	 	)

	 	//Inclusion
	 		//specifies the inclusion of a field. Non-zero integers are also treated as true

	 		db.users.find(
	 			{},
	 			{
	 				"firstName":1,
	 				"lastName":1,
	 				"_id": 0
	 			}
	 		)

	 		//with query
	 		db.users.find(
	 				{
	 					"age" : {$gte:50}
	 				},
	 				{
	 					"_id" : 0,
	 					"firstName": 1,
	 				}
	 		)

	 		//look for documents under hr dept and display only their first and lastname plus phone number
	 		db.users.find(
			{
				"department" : "HR"
			},
			{
				"_id" : 0,
				"firstName": 1,
				"lastName": 1,
				"contact.phone":1
			}
		)